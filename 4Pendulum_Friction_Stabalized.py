from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from sys import *

#  from pyquaternion import Quaternion    ## would be useful for 3D simulation
import numpy as np
from math import *

window = 0  # number of the glut window
theta = 0.0
simTime = 0
dT = 0.01
simRun = True
RAD_TO_DEG = 180.0 / 3.1416


#####################################################
#### Link class, i.e., for a rigid body
#####################################################

class Link:
    color = [0, 0, 0]  ## draw color
    size = [1, 1, 1]  ## dimensions
    mass = 1.0  ## mass in kg
    izz = 1.0  ## moment of inertia about z-axis
    theta = [0, 0, 0]
    omega = 0  ## 2D angular velocity
    posn = np.array([0.0, 0.0, 0.0])  ## 3D position (keep z=0 for 2D)
    vel = np.array([0.0, 0.0, 0.0])  ## initial velocity

    def draw(self):  ### steps to draw a link
        glPushMatrix()  ## save copy of coord frame
        glTranslatef(self.posn[0], self.posn[1], self.posn[2])  ## move
        glRotatef(self.theta * RAD_TO_DEG, 0, 0, 1)  ## rotate
        glScale(self.size[0], self.size[1], self.size[2])  ## set size
        glColor3f(self.color[0], self.color[1], self.color[2])  ## set colour
        DrawCube()  ## draw a scaled cube
        glPopMatrix()  ## restore old coord frame


#####################################################
#### main():   launches app
#####################################################

def main():
    global window
    global link
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)  # display mode
    glutInitWindowSize(640, 480)  # window size
    glutInitWindowPosition(0, 0)  # window coords for mouse start at top-left
    window = glutCreateWindow("CPSC 526 Simulation Template")
    glutDisplayFunc(DrawWorld)  # register the function to draw the world
    glutFullScreen()               # full screen
    glutIdleFunc(SimWorld)  # when doing nothing, redraw the scene
    glutReshapeFunc(ReSizeGLScene)  # register the function to call when window is resized
    glutKeyboardFunc(keyPressed)  # register the function to call when keyboard is pressed
    InitGL(640, 480)  # initialize window

    link = []
    link.append(Link());
    link.append(Link());
    link.append(Link());
    link.append(Link());

    resetSim()

    glutMainLoop()  # start event processing loop


#####################################################
#### keyPressed():  called whenever a key is pressed
#####################################################

def resetSim():
    # global link1, link[1], link3, link4
    global link
    global simTime, simRun

    printf("Simulation reset\n")
    simRun = True
    simTime = 0

    link[0].size = [0.01, 1.0, 0.01]
    link[0].color = [1, 0.9, 0.9]
    link[0].posn = np.array([0.0, 2.0, 0.0])
    link[0].vel = np.array([0.0, 0.0, 0.0])
    link[0].theta = 0
    link[0].omega = 0  ## radians per second

    link[1].size = [0.01, 1.0, 0.01]
    link[1].color = [1, 0.9, 0.9]
    link[1].posn = np.array([0.0, 1.0, 0.0])
    link[1].vel = np.array([0.0, 0.0, 0.0])
    link[1].theta = 0
    link[1].omega = 0  ## radians per second

    link[2].size = [0.01, 1.0, 0.01]
    link[2].color = [1, 0.9, 0.9]
    link[2].posn = np.array([0.5 * sin(pi / 4), (0.5 - 0.5 * cos(pi / 4)), 0.0])
    link[2].vel = np.array([0.0, 0.0, 0.0])
    link[2].theta = pi/4
    link[2].omega = 0  ## radians per second

    link[3].size = [0.01, 1.0, 0.01]
    link[3].color = [1, 0.9, 0.9]
    link[3].posn = np.array([1.5 * sin(pi / 4), -1 + (1.5 - 1.5 * cos(pi / 4)), 0.0])
    link[3].vel = np.array([0.0, 0.0, 0.0])
    link[3].theta = pi / 4
    link[3].omega = 0  ## radians per second

#####################################################
#### keyPressed():  called whenever a key is pressed
#####################################################

def keyPressed(key, x, y):
    global simRun
    ch = key.decode("utf-8")
    if ch == ' ':  #### toggle the simulation
        if (simRun == True):
            simRun = False
        else:
            simRun = True
    elif ch == chr(27):  #### ESC key
        sys.exit()
    elif ch == 'q':  #### quit
        sys.exit()
    elif ch == 'r':  #### reset simulation
        resetSim()


#####################################################
#### SimWorld():  simulates a time step
#####################################################
def Rot(theta):
    rz = np.array([[cos(theta), -sin(theta), 0], [sin(theta), cos(theta), 0], [0, 0, 1]])
    return rz

def shapeMat(mat1, mat2, mat3):
    tstack = np.hstack((np.hstack((mat1[0][:], mat2[0][:])), mat3[0][:]))
    for i in range(1, mat1.shape[0]):
        tstack = np.vstack((tstack, np.hstack((np.hstack((mat1[i][:], mat2[i][:])), mat3[i][:]))))
    return tstack

def stackfun(mat1, mat2):
    tstack = np.hstack((mat1[0, :], mat2[0, :]))
    for j in range(1, mat1.shape[0]):
        tstack = np.vstack((tstack, np.hstack((mat1[j, :], mat2[j, :]))))
    return tstack

def Energy(Inertia, Mass, distance, Omega, Height):
    G = [0, -9.8, 0]
    # KE = 0.5 * (Inertia + Mass * (distance ** 2)) * (np.matmul(np.transpose(Omega), Omega))
    KE = np.matmul(np.matmul(np.transpose(Omega), Inertia), Omega)
    U = abs(Mass[0, 0] * G[1] * Height)
    return KE, U

def Jfunc(rMat1, rMat2):
    J = np.array([[1, 0, 0, rMat1[0][0], rMat1[0][1], rMat1[0][2], -1, 0, 0, rMat2[0][0], rMat2[0][1], rMat2[0][2]],
                   [0, 1, 0, rMat1[1][0], rMat1[1][1], rMat1[1][2], 0, -1, 0, rMat2[1][0], rMat2[1][1], rMat2[1][2]],
                   [0, 0, 1, rMat1[2][0], rMat1[2][1], rMat1[2][2], 0, 0, -1, rMat2[2][0], rMat2[2][1], rMat2[2][2]]])
    return J

def constructRmat(rW):
    return np.array([[0, -1 * rW[2], rW[1]], [rW[2], 0, -1 * rW[0]], [-1 * rW[1], rW[0], 0]])

def SimWorld():
    global simTime, dT, simRun
    global link

    deltaTheta = 2.4
    if (simRun == False):  ## is simulation stopped?
        return

    M = np.array([[link[0].mass, 0, 0], [0, link[0].mass, 0], [0, 0, link[0].mass]])

    I_Lxx = (link[0].mass / 12) * (link[0].size[0] ** 2 + link[0].size[2] ** 2)
    I_Lyy = (link[0].mass / 12) * (link[0].size[1] ** 2 + link[0].size[2] ** 2)
    I_Lzz = (link[0].mass / 12) * (link[0].size[0] ** 2 + link[0].size[1] ** 2)

    I_L = np.array([[I_Lxx, 0, 0], [0, I_Lyy, 0], [0, 0, I_Lzz]])
    
    I_W1 = np.matmul(Rot(link[0].theta), np.matmul(I_L, np.linalg.inv(Rot(link[0].theta))))
    I_W2 = np.matmul(Rot(link[1].theta), np.matmul(I_L, np.linalg.inv(Rot(link[1].theta))))
    I_W3 = np.matmul(Rot(link[2].theta), np.matmul(I_L, np.linalg.inv(Rot(link[2].theta))))
    I_W4 = np.matmul(Rot(link[3].theta), np.matmul(I_L, np.linalg.inv(Rot(link[3].theta))))

    G = np.array([0.0, -9.8, 0.0])
    
    W1 = np.array([0, 0, link[0].omega])
    W2 = np.array([0, 0, link[1].omega])
    W3 = np.array([0, 0, link[2].omega])
    W4 = np.array([0, 0, link[3].omega])
    
    rL = np.array([0.0, 0.5, 0.0])
    rW = np.array([
        np.array([-0.5 * sin(link[0].theta), 0.5 * cos(link[0].theta), 0]),
        np.array([-0.5 * sin(link[1].theta), 0.5 * cos(link[1].theta), 0]),
        np.array([-0.5 * sin(link[2].theta), 0.5 * cos(link[2].theta), 0]),
        np.array([-0.5 * sin(link[3].theta), 0.5 * cos(link[3].theta), 0])
    ])
    # rW = np.array([
    #     np.matmul(Rot(link[0].theta), rL),
    #     np.matmul(Rot(link[1].theta), rL),
    #     np.matmul(Rot(link[2].theta), rL),
    #     np.matmul(Rot(link[3].theta), rL)
    # ])

    rMat1 = constructRmat(rW[0])
    rMat2 = constructRmat(rW[1])
    rMat3 = constructRmat(rW[2])
    rMat4 = constructRmat(rW[3])

    J1 = np.array([[-1, 0, 0, rMat1[0][0], rMat1[0][1], rMat1[0][2]],
                  [0, -1, 0, rMat1[1][0], rMat1[1][1], rMat1[1][2]],
                  [0, 0, -1, rMat1[2][0], rMat1[2][1], rMat1[2][2]]])

    J2 = Jfunc(rMat1, rMat2)
    J3 = Jfunc(rMat2, rMat3)
    J4 = Jfunc(rMat3, rMat4)

    JT1 = np.transpose(J1)
    JT2 = np.transpose(J2)
    JT3 = np.transpose(J3)
    JT4 = np.transpose(J4)
    
    kd = [-0.3, -0.3, -0.3, -0.3]
    damp = np.array([
        kd[0] * W1,
        kd[1] * W2,
        kd[2] * W3,
        kd[3] * W4
    ])
    
    deltaP = np.array([
        link[0].posn + rW[0] - [0.0, 1.5, 0.0],
        link[1].posn + rW[1] - link[0].posn + rW[0],
        link[2].posn + rW[2] - link[1].posn + rW[1],
        link[3].posn + rW[3] - link[2].posn + rW[2]
    ])
    deltaV = np.array([
        link[0].vel + np.cross(W1, rL),
        link[1].vel + np.cross(W2, rL) - link[0].vel - np.cross(W1, rL),
        link[2].vel + np.cross(W3, rL) - link[1].vel - np.cross(W2, rL),
        link[3].vel + np.cross(W4, rL) - link[2].vel - np.cross(W3, rL)
    ])
    k_p = [1, 1, 1, 1]
    k_d = [0, 0, 0, 0]

    stablize = np.array([
        k_p[0] * deltaP[0] + k_d[0] * deltaV[0],
        k_p[1] * deltaP[1] + k_d[1] * deltaV[1],
        k_p[2] * deltaP[2] + k_d[2] * deltaV[2],
        k_p[3] * deltaP[3] + k_d[3] * deltaV[3]
    ])

    y0 = [0.0, -1.4, 0.0]
    y = link[3].posn - rW[3]
    ydot = link[3].vel - np.cross(W4, rL)
    kp_param, kd_param = 100, 100
    yForce = kp_param * (y0 - ydot) - kd_param * ydot

    a1 = np.vstack([
        np.vstack([
            stackfun(
                shapeMat(
                    np.vstack([stackfun(M, np.zeros((3, 3))), stackfun(np.zeros((3, 3)), I_W1)]),
                    np.zeros((6, 6)),
                    np.zeros((6, 6))
                ),
                shapeMat(
                    np.zeros((6, 6)),
                    np.vstack([stackfun(JT1[0:3, :], JT2[0:3, :]), stackfun(JT1[3:6, :], JT2[3:6, :])]),
                    np.zeros((6, 6))
                )
            ),
            stackfun(
                shapeMat(
                    np.zeros((6, 6)),
                    np.vstack([stackfun(M, np.zeros((3, 3))), stackfun(np.zeros((3, 3)), I_W2)]),
                    np.zeros((6, 6))
                ),
                shapeMat(
                    np.zeros((6, 6)),
                    np.vstack([stackfun(np.zeros((3, 3)), JT2[6:9, :]), stackfun(np.zeros((3, 3)), JT2[9:12, :])]),
                    np.vstack([stackfun(JT3[0:3, :], np.zeros((3, 3))), stackfun(JT3[3:6, :], np.zeros((3, 3)))])
                )
            )
        ]),
        stackfun(
            shapeMat(
                np.zeros((6, 6)),
                np.zeros((6, 6)),
                np.vstack([stackfun(M, np.zeros((3, 3))), stackfun(np.zeros((3, 3)), I_W3)])
            ),
            shapeMat(
                np.zeros((6, 6)),
                np.zeros((6, 6)),
                np.vstack([stackfun(JT3[6:9, :], JT4[0:3, :]), stackfun(JT3[9:12, :], JT4[3:6, :])])
            )
        )
    ])

    a2 = np.vstack([
        np.vstack([
            stackfun(
                shapeMat(
                    np.zeros((6, 6)),
                    np.zeros((6, 6)),
                    np.zeros((6, 6))
                ),
                shapeMat(
                    np.vstack([stackfun(M, np.zeros((3, 3))), stackfun(np.zeros((3, 3)), I_W4)]),
                    np.zeros((6, 6)),
                    np.vstack([stackfun(np.zeros((3, 3)), JT4[6:9, :]), stackfun(np.zeros((3, 3)), JT4[9:12, :])])
                )
            ),
            stackfun(
                shapeMat(
                    np.vstack([J1[:, 0:6], J2[:, 0:6]]),
                    np.vstack([np.zeros((3, 6)), J2[:, 6:12]]),
                    np.zeros((6, 6))
                ),
                shapeMat(
                    np.zeros((6, 6)),
                    np.zeros((6, 6)),
                    np.zeros((6, 6))
                )
            )
        ]),
        stackfun(
            shapeMat(
                np.zeros((6, 6)),
                np.vstack([J3[:, 0:6], np.zeros((3, 6))]),
                np.vstack([J3[:, 6:12], J4[:, 0:6]])
            ),
            shapeMat(
                np.vstack([np.zeros((3, 6)), J4[:, 6:12]]),
                np.zeros((6, 6)),
                np.zeros((6, 6))
            )
        )
    ])
    a = np.vstack([
        a1,
        a2
    ])
    
    b = np.array([np.matmul(M, G)[0],
                  np.matmul(M, G)[1],
                  np.matmul(M, G)[2],
                  damp[0, 0],
                  damp[0, 1],
                  damp[0, 2],
                  np.matmul(M, G)[0],
                  np.matmul(M, G)[1],
                  np.matmul(M, G)[2],
                  damp[1, 0],
                  damp[1, 1],
                  damp[1, 2],
                  np.matmul(M, G)[0],
                  np.matmul(M, G)[1],
                  np.matmul(M, G)[2],
                  damp[2, 0],
                  damp[2, 1],
                  damp[2, 2],
                  np.matmul(M, G)[0],
                  np.matmul(M, G)[1],
                  np.matmul(M, G)[2],
                  damp[3, 0],
                  damp[3, 1],
                  damp[3, 2],
                  np.cross(W1, np.cross(W1, rW[0]))[0],
                  np.cross(W1, np.cross(W1, rW[0]))[1],
                  np.cross(W1, np.cross(W1, rW[0]))[2],
                  np.cross(W1, np.cross(W1, rW[0]))[0] + np.cross(W2, np.cross(W2, rW[1]))[0] + stablize[1, 0],
                  np.cross(W1, np.cross(W1, rW[0]))[1] + np.cross(W2, np.cross(W2, rW[1]))[1] + stablize[1, 1],
                  np.cross(W1, np.cross(W1, rW[0]))[2] + np.cross(W2, np.cross(W2, rW[1]))[2] + stablize[1, 2],
                  np.cross(W2, np.cross(W2, rW[1]))[0] + np.cross(W3, np.cross(W3, rW[2]))[0] + stablize[2, 0],
                  np.cross(W2, np.cross(W2, rW[1]))[1] + np.cross(W3, np.cross(W3, rW[2]))[1] + stablize[2, 1],
                  np.cross(W2, np.cross(W2, rW[1]))[2] + np.cross(W3, np.cross(W3, rW[2]))[2] + stablize[2, 2],
                  np.cross(W3, np.cross(W3, rW[2]))[0] + np.cross(W4, np.cross(W4, rW[3]))[0] + stablize[3, 0],
                  np.cross(W3, np.cross(W3, rW[2]))[1] + np.cross(W4, np.cross(W4, rW[3]))[1] + stablize[3, 1],
                  np.cross(W3, np.cross(W3, rW[2]))[2] + np.cross(W4, np.cross(W4, rW[3]))[2] + stablize[3, 2]
                  ])

    x = np.linalg.solve(a, b)

    en = Energy(I_L, M, 1/2 * (link[0].size[1]), W1, y[1] - y0[1])
    print("\rEnergy: KE = %f U = %f Total = %f" % (en[0], en[1], en[0]+en[1]), end='')
    
    #### explicit Euler integration to update the state
    link[0].posn += link[0].vel * dT
    link[0].vel += x[0:3] * dT
    link[0].theta += link[0].omega * dT
    link[0].omega += (x[5] * dT)

    link[1].posn += link[1].vel * dT
    link[1].vel += x[6:9] * dT
    link[1].theta += link[1].omega * dT
    link[1].omega += (x[11] * dT)

    link[2].posn += link[2].vel * dT
    link[2].vel += x[12:15] * dT
    link[2].theta += link[2].omega * dT
    link[2].omega += (x[17] * dT)

    link[3].posn += link[3].vel * dT
    link[3].vel += x[18:21] * dT
    link[3].theta += link[3].omega * dT
    link[3].omega += (x[23] * dT)

    simTime += dT

    #### draw the updated state
    DrawWorld()
    # printf("simTime=%.2f\n", simTime)


#####################################################
#### DrawWorld():  draw the world
#####################################################

def DrawWorld():
    global link
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  # Clear The Screen And The Depth Buffer
    glLoadIdentity();
    gluLookAt(3, 2, 10, 0, 2, 0, 0, 1, 0)

    DrawOrigin()
    link[0].draw()
    link[1].draw()
    link[2].draw()
    link[3].draw()

    # DrawGround()

    glutSwapBuffers()  # swap the buffers to display what was just drawn


#####################################################
#### initGL():  does standard OpenGL initialization work
#####################################################

def InitGL(Width, Height):  # We call this right after our OpenGL window is created.
    glClearColor(1.0, 1.0, 0.9, 0.0)  # This Will Clear The Background Color To Black
    glClearDepth(1.0)  # Enables Clearing Of The Depth Buffer
    glDepthFunc(GL_LESS)  # The Type Of Depth Test To Do
    glEnable(GL_DEPTH_TEST)  # Enables Depth Testing
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LINE_SMOOTH);
    glShadeModel(GL_SMOOTH)  # Enables Smooth Color Shading
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()  # Reset The Projection Matrix
    gluPerspective(45.0, float(Width) / float(Height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)


#####################################################
#### ReSizeGLScene():    called when window is resized
#####################################################

def ReSizeGLScene(Width, Height):
    if Height == 0:  # Prevent A Divide By Zero If The Window Is Too Small
        Height = 1
    glViewport(0, 0, Width, Height)  # Reset The Current Viewport And Perspective Transformation
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(Width) / float(Height), 0.1,
                   100.0)  ## 45 deg horizontal field of view, aspect ratio, near, far
    glMatrixMode(GL_MODELVIEW)


#####################################################
#### DrawOrigin():  draws RGB lines for XYZ origin of coordinate system
#####################################################

def DrawOrigin():
    glLineWidth(3.0);

    glColor3f(1, 0.5, 0.5)  ## light red x-axis
    glBegin(GL_LINES)
    glVertex3f(0, 0, 0)
    glVertex3f(1, 0, 0)
    glEnd()

    glColor3f(0.5, 1, 0.5)  ## light green y-axis
    glBegin(GL_LINES)
    glVertex3f(0, 0, 0)
    glVertex3f(0, 1, 0)
    glEnd()

    glColor3f(0.5, 0.5, 1)  ## light blue z-axis
    glBegin(GL_LINES)
    glVertex3f(0, 0, 0)
    glVertex3f(0, 0, 1)
    glEnd()


#####################################################
#### DrawCube():  draws a cube that spans from (-1,-1,-1) to (1,1,1)
#####################################################

def DrawCube():
    glScalef(0.5, 0.5, 0.5);  # dimensions below are for a 2x2x2 cube, so scale it down by a half first
    glBegin(GL_QUADS);  # Start Drawing The Cube

    glVertex3f(1.0, 1.0, -1.0);  # Top Right Of The Quad (Top)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Left Of The Quad (Top)
    glVertex3f(-1.0, 1.0, 1.0);  # Bottom Left Of The Quad (Top)
    glVertex3f(1.0, 1.0, 1.0);  # Bottom Right Of The Quad (Top)

    glVertex3f(1.0, -1.0, 1.0);  # Top Right Of The Quad (Bottom)
    glVertex3f(-1.0, -1.0, 1.0);  # Top Left Of The Quad (Bottom)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Bottom)
    glVertex3f(1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Bottom)

    glVertex3f(1.0, 1.0, 1.0);  # Top Right Of The Quad (Front)
    glVertex3f(-1.0, 1.0, 1.0);  # Top Left Of The Quad (Front)
    glVertex3f(-1.0, -1.0, 1.0);  # Bottom Left Of The Quad (Front)
    glVertex3f(1.0, -1.0, 1.0);  # Bottom Right Of The Quad (Front)

    glVertex3f(1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Back)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Back)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Right Of The Quad (Back)
    glVertex3f(1.0, 1.0, -1.0);  # Top Left Of The Quad (Back)

    glVertex3f(-1.0, 1.0, 1.0);  # Top Right Of The Quad (Left)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Left Of The Quad (Left)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Left)
    glVertex3f(-1.0, -1.0, 1.0);  # Bottom Right Of The Quad (Left)

    glVertex3f(1.0, 1.0, -1.0);  # Top Right Of The Quad (Right)
    glVertex3f(1.0, 1.0, 1.0);  # Top Left Of The Quad (Right)
    glVertex3f(1.0, -1.0, 1.0);  # Bottom Left Of The Quad (Right)
    glVertex3f(1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Right)
    glEnd();  # Done Drawing The Quad

    ### Draw the wireframe edges
    glColor3f(0.0, 0.0, 0.0);
    glLineWidth(1.0);

    glBegin(GL_LINE_LOOP);
    glVertex3f(1.0, 1.0, -1.0);  # Top Right Of The Quad (Top)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Left Of The Quad (Top)
    glVertex3f(-1.0, 1.0, 1.0);  # Bottom Left Of The Quad (Top)
    glVertex3f(1.0, 1.0, 1.0);  # Bottom Right Of The Quad (Top)
    glEnd();  # Done Drawing The Quad

    glBegin(GL_LINE_LOOP);
    glVertex3f(1.0, -1.0, 1.0);  # Top Right Of The Quad (Bottom)
    glVertex3f(-1.0, -1.0, 1.0);  # Top Left Of The Quad (Bottom)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Bottom)
    glVertex3f(1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Bottom)
    glEnd();  # Done Drawing The Quad

    glBegin(GL_LINE_LOOP);
    glVertex3f(1.0, 1.0, 1.0);  # Top Right Of The Quad (Front)
    glVertex3f(-1.0, 1.0, 1.0);  # Top Left Of The Quad (Front)
    glVertex3f(-1.0, -1.0, 1.0);  # Bottom Left Of The Quad (Front)
    glVertex3f(1.0, -1.0, 1.0);  # Bottom Right Of The Quad (Front)
    glEnd();  # Done Drawing The Quad

    glBegin(GL_LINE_LOOP);
    glVertex3f(1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Back)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Back)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Right Of The Quad (Back)
    glVertex3f(1.0, 1.0, -1.0);  # Top Left Of The Quad (Back)
    glEnd();  # Done Drawing The Quad

    glBegin(GL_LINE_LOOP);
    glVertex3f(-1.0, 1.0, 1.0);  # Top Right Of The Quad (Left)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Left Of The Quad (Left)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Left)
    glVertex3f(-1.0, -1.0, 1.0);  # Bottom Right Of The Quad (Left)
    glEnd();  # Done Drawing The Quad

    glBegin(GL_LINE_LOOP);
    glVertex3f(1.0, 1.0, -1.0);  # Top Right Of The Quad (Right)
    glVertex3f(1.0, 1.0, 1.0);  # Top Left Of The Quad (Right)
    glVertex3f(1.0, -1.0, 1.0);  # Bottom Left Of The Quad (Right)
    glVertex3f(1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Right)
    glEnd();  # Done Drawing The Quad


def DrawGround():
    glColor3f(1, 0.5, 0.5)
    glBegin(GL_QUADS)
    glVertex3f(1, -1.4, -1)
    glVertex3f(1, -1.4, 1)
    glVertex3f(-1, -1.4, 1)
    glVertex3f(-1, -1.4, -1)
    glEnd()
####################################################
# printf()
####################################################

def printf(format, *args):
    sys.stdout.write(format % args)


################################################################################
# start the app

print("Hit ESC key to quit.")
main()
