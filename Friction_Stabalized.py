from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from sys import *

#  from pyquaternion import Quaternion    ## would be useful for 3D simulation
import numpy as np
from math import *

window = 0  # number of the glut window
theta = 0.0
simTime = 0
dT = 0.01
simRun = True
RAD_TO_DEG = 180.0 / 3.1416


#####################################################
#### Link class, i.e., for a rigid body
#####################################################

class Link:
    color = [0, 0, 0]  ## draw color
    size = [1, 1, 1]  ## dimensions
    mass = 1.0  ## mass in kg
    izz = 1.0  ## moment of inertia about z-axis
    theta = [0, 0, 0]
    omega = 0  ## 2D angular velocity
    posn = np.array([0.0, 0.0, 0.0])  ## 3D position (keep z=0 for 2D)
    vel = np.array([0.0, 0.0, 0.0])  ## initial velocity

    def draw(self):  ### steps to draw a link
        glPushMatrix()  ## save copy of coord frame
        glTranslatef(self.posn[0], self.posn[1], self.posn[2])  ## move
        glRotatef(self.theta * RAD_TO_DEG, 0, 0, 1)  ## rotate
        glScale(self.size[0], self.size[1], self.size[2])  ## set size
        glColor3f(self.color[0], self.color[1], self.color[2])  ## set colour
        DrawCube()  ## draw a scaled cube
        glPopMatrix()  ## restore old coord frame


#####################################################
#### main():   launches app
#####################################################

def main():
    global window
    global link1, link2, link3, link4
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)  # display mode
    glutInitWindowSize(640, 480)  # window size
    glutInitWindowPosition(0, 0)  # window coords for mouse start at top-left
    window = glutCreateWindow("CPSC 526 Simulation Template")
    glutDisplayFunc(DrawWorld)  # register the function to draw the world
    glutFullScreen()               # full screen
    glutIdleFunc(SimWorld)  # when doing nothing, redraw the scene
    glutReshapeFunc(ReSizeGLScene)  # register the function to call when window is resized
    glutKeyboardFunc(keyPressed)  # register the function to call when keyboard is pressed
    InitGL(640, 480)  # initialize window

    link1 = Link();

    resetSim()

    glutMainLoop()  # start event processing loop


#####################################################
#### keyPressed():  called whenever a key is pressed
#####################################################

def resetSim():
    global link1
    global simTime, simRun

    printf("Simulation reset\n")
    simRun = True
    simTime = 0

    link1.size = [0.01, 1.0, 0.01]
    link1.color = [1, 0.9, 0.9]
    link1.posn = np.array([0.0, 0.0, 0.0])
    link1.vel = np.array([0.0, 0.0, 0.0])
    link1.theta = pi/4
    link1.omega = 0  ## radians per second


#####################################################
#### keyPressed():  called whenever a key is pressed
#####################################################

def keyPressed(key, x, y):
    global simRun
    ch = key.decode("utf-8")
    if ch == ' ':  #### toggle the simulation
        if (simRun == True):
            simRun = False
        else:
            simRun = True
    elif ch == chr(27):  #### ESC key
        sys.exit()
    elif ch == 'q':  #### quit
        sys.exit()
    elif ch == 'r':  #### reset simulation
        resetSim()

def Rot(theta):
    rz = np.array([[cos(theta), -sin(theta), 0], [sin(theta), cos(theta), 0], [0, 0, 1]])
    return rz

def shapeMat(mat1, mat2, mat3):
    tstack = np.hstack((np.hstack((mat1[0][:], mat2[0][:])), mat3[0][:]))
    for i in range(1, mat1.shape[0]):
        tstack = np.vstack((tstack, np.hstack((np.hstack((mat1[i][:], mat2[i][:])), mat3[i][:]))))
    return tstack

def Energy(Inertia, Mass, Omega, Height):
    G = [0, -9.8, 0]
    KE = np.matmul(np.matmul(np.transpose(Omega), Inertia), Omega)
    U = abs(Mass[0, 0] * G[1] * Height)
    return KE, U

#####################################################
#### SimWorld():  simulates a time step
#####################################################
def SimWorld():
    global simTime, dT, simRun
    global link1, link2

    if (simRun == False):
        return

    M = np.array([[link1.mass, 0, 0], [0, link1.mass, 0], [0, 0, link1.mass]])

    I_Lxx = (link1.mass / 12) * (link1.size[0] ** 2 + link1.size[2] ** 2)
    I_Lyy = (link1.mass / 12) * (link1.size[1] ** 2 + link1.size[2] ** 2)
    I_Lzz = (link1.mass / 12) * (link1.size[0] ** 2 + link1.size[1] ** 2)

    I_L = np.array([[I_Lxx, 0, 0], [0, I_Lyy, 0], [0, 0, I_Lzz]])
    I_W1 = np.matmul(Rot(link1.theta), np.matmul(I_L, np.linalg.inv(Rot(link1.theta))))

    G = np.array([0.0, -9.8, 0.0])

    W1 = np.array([0, 0, link1.omega])

    rL = np.array([0.0, 0.5, 0.0])
    rW1 = np.array([-0.5 * sin(link1.theta), 0.5 * cos(link1.theta), 0])

    rMat1 = np.array([[0, -1 * rW1[2], rW1[1]], [rW1[2], 0, -1 * rW1[0]], [-1 * rW1[1], rW1[0], 0]])

    J1 = np.array([[-1, 0, 0, rMat1[0][0], rMat1[0][1], rMat1[0][2]],
                  [0, -1, 0, rMat1[1][0], rMat1[1][1], rMat1[1][2]],
                  [0, 0, -1, rMat1[2][0], rMat1[2][1], rMat1[2][2]]])

    JT1 = np.transpose(J1)

    kd1 = -0.1
    damp1 = kd1 * W1

    deltaP1 = link1.posn + rW1 - [-0.5 * sin(pi/4), 0.5 * cos(pi/4), 0]
    deltaV1 = link1.vel + np.cross(W1, rL)
    k_p1 = 0.4
    k_d1 = 0
    stablize1 = k_p1 * deltaP1 + k_d1 * deltaV1

    a = np.vstack((np.vstack((shapeMat(M, np.zeros((3, 3)), JT1[0:3, :]),
                   shapeMat(np.zeros((3, 3)), I_W1, JT1[3:6, :]))),
                   shapeMat(J1[:, 0:3], J1[:, 3:6], np.zeros((3, 3)))))

    b = np.array([np.matmul(M, G)[0],
                  np.matmul(M, G)[1],
                  np.matmul(M, G)[2],
                  damp1[0],
                  damp1[1],
                  damp1[2],
                  np.cross(W1, np.cross(W1, rW1))[0] + stablize1[0],
                  np.cross(W1, np.cross(W1, rW1))[1] + stablize1[1],
                  np.cross(W1, np.cross(W1, rW1))[2] + stablize1[2]
                 ])

    x = np.linalg.solve(a, b)

    y0 = [0.0, -1.4, 0.0]
    y = link1.posn - rW1
    en = Energy(I_L, M, W1, y[1] - y0[1])
    print("\rEnergy: KE = %f U = %f Total = %f" % (en[0], en[1], en[0] + en[1]), end='')

    #### explicit Euler integration to update the state
    link1.posn += link1.vel * dT
    link1.vel += x[0:3] * dT
    link1.theta += link1.omega * dT
    link1.omega += (x[5] * dT)

    simTime += dT

    #### draw the updated state
    DrawWorld()
    # printf("simTime=%.2f\n", simTime)


#####################################################
#### DrawWorld():  draw the world
#####################################################

def DrawWorld():
    global link1, link2

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  # Clear The Screen And The Depth Buffer
    glLoadIdentity();
    gluLookAt(1, 1, 3, 0, 0, 0, 0, 1, 0)

    DrawOrigin()
    link1.draw()

    glutSwapBuffers()  # swap the buffers to display what was just drawn


#####################################################
#### initGL():  does standard OpenGL initialization work
#####################################################

def InitGL(Width, Height):  # We call this right after our OpenGL window is created.
    glClearColor(1.0, 1.0, 0.9, 0.0)  # This Will Clear The Background Color To Black
    glClearDepth(1.0)  # Enables Clearing Of The Depth Buffer
    glDepthFunc(GL_LESS)  # The Type Of Depth Test To Do
    glEnable(GL_DEPTH_TEST)  # Enables Depth Testing
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LINE_SMOOTH);
    glShadeModel(GL_SMOOTH)  # Enables Smooth Color Shading
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()  # Reset The Projection Matrix
    gluPerspective(45.0, float(Width) / float(Height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)


#####################################################
#### ReSizeGLScene():    called when window is resized
#####################################################

def ReSizeGLScene(Width, Height):
    if Height == 0:  # Prevent A Divide By Zero If The Window Is Too Small
        Height = 1
    glViewport(0, 0, Width, Height)  # Reset The Current Viewport And Perspective Transformation
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(Width) / float(Height), 0.1,
                   100.0)  ## 45 deg horizontal field of view, aspect ratio, near, far
    glMatrixMode(GL_MODELVIEW)


#####################################################
#### DrawOrigin():  draws RGB lines for XYZ origin of coordinate system
#####################################################

def DrawOrigin():
    glLineWidth(3.0);

    glColor3f(1, 0.5, 0.5)  ## light red x-axis
    glBegin(GL_LINES)
    glVertex3f(0, 0, 0)
    glVertex3f(1, 0, 0)
    glEnd()

    glColor3f(0.5, 1, 0.5)  ## light green y-axis
    glBegin(GL_LINES)
    glVertex3f(0, 0, 0)
    glVertex3f(0, 1, 0)
    glEnd()

    glColor3f(0.5, 0.5, 1)  ## light blue z-axis
    glBegin(GL_LINES)
    glVertex3f(0, 0, 0)
    glVertex3f(0, 0, 1)
    glEnd()


#####################################################
#### DrawCube():  draws a cube that spans from (-1,-1,-1) to (1,1,1)
#####################################################

def DrawCube():
    glScalef(0.5, 0.5, 0.5);  # dimensions below are for a 2x2x2 cube, so scale it down by a half first
    glBegin(GL_QUADS);  # Start Drawing The Cube

    glVertex3f(1.0, 1.0, -1.0);  # Top Right Of The Quad (Top)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Left Of The Quad (Top)
    glVertex3f(-1.0, 1.0, 1.0);  # Bottom Left Of The Quad (Top)
    glVertex3f(1.0, 1.0, 1.0);  # Bottom Right Of The Quad (Top)

    glVertex3f(1.0, -1.0, 1.0);  # Top Right Of The Quad (Bottom)
    glVertex3f(-1.0, -1.0, 1.0);  # Top Left Of The Quad (Bottom)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Bottom)
    glVertex3f(1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Bottom)

    glVertex3f(1.0, 1.0, 1.0);  # Top Right Of The Quad (Front)
    glVertex3f(-1.0, 1.0, 1.0);  # Top Left Of The Quad (Front)
    glVertex3f(-1.0, -1.0, 1.0);  # Bottom Left Of The Quad (Front)
    glVertex3f(1.0, -1.0, 1.0);  # Bottom Right Of The Quad (Front)

    glVertex3f(1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Back)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Back)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Right Of The Quad (Back)
    glVertex3f(1.0, 1.0, -1.0);  # Top Left Of The Quad (Back)

    glVertex3f(-1.0, 1.0, 1.0);  # Top Right Of The Quad (Left)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Left Of The Quad (Left)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Left)
    glVertex3f(-1.0, -1.0, 1.0);  # Bottom Right Of The Quad (Left)

    glVertex3f(1.0, 1.0, -1.0);  # Top Right Of The Quad (Right)
    glVertex3f(1.0, 1.0, 1.0);  # Top Left Of The Quad (Right)
    glVertex3f(1.0, -1.0, 1.0);  # Bottom Left Of The Quad (Right)
    glVertex3f(1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Right)
    glEnd();  # Done Drawing The Quad

    ### Draw the wireframe edges
    glColor3f(0.0, 0.0, 0.0);
    glLineWidth(1.0);

    glBegin(GL_LINE_LOOP);
    glVertex3f(1.0, 1.0, -1.0);  # Top Right Of The Quad (Top)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Left Of The Quad (Top)
    glVertex3f(-1.0, 1.0, 1.0);  # Bottom Left Of The Quad (Top)
    glVertex3f(1.0, 1.0, 1.0);  # Bottom Right Of The Quad (Top)
    glEnd();  # Done Drawing The Quad

    glBegin(GL_LINE_LOOP);
    glVertex3f(1.0, -1.0, 1.0);  # Top Right Of The Quad (Bottom)
    glVertex3f(-1.0, -1.0, 1.0);  # Top Left Of The Quad (Bottom)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Bottom)
    glVertex3f(1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Bottom)
    glEnd();  # Done Drawing The Quad

    glBegin(GL_LINE_LOOP);
    glVertex3f(1.0, 1.0, 1.0);  # Top Right Of The Quad (Front)
    glVertex3f(-1.0, 1.0, 1.0);  # Top Left Of The Quad (Front)
    glVertex3f(-1.0, -1.0, 1.0);  # Bottom Left Of The Quad (Front)
    glVertex3f(1.0, -1.0, 1.0);  # Bottom Right Of The Quad (Front)
    glEnd();  # Done Drawing The Quad

    glBegin(GL_LINE_LOOP);
    glVertex3f(1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Back)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Back)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Right Of The Quad (Back)
    glVertex3f(1.0, 1.0, -1.0);  # Top Left Of The Quad (Back)
    glEnd();  # Done Drawing The Quad

    glBegin(GL_LINE_LOOP);
    glVertex3f(-1.0, 1.0, 1.0);  # Top Right Of The Quad (Left)
    glVertex3f(-1.0, 1.0, -1.0);  # Top Left Of The Quad (Left)
    glVertex3f(-1.0, -1.0, -1.0);  # Bottom Left Of The Quad (Left)
    glVertex3f(-1.0, -1.0, 1.0);  # Bottom Right Of The Quad (Left)
    glEnd();  # Done Drawing The Quad

    glBegin(GL_LINE_LOOP);
    glVertex3f(1.0, 1.0, -1.0);  # Top Right Of The Quad (Right)
    glVertex3f(1.0, 1.0, 1.0);  # Top Left Of The Quad (Right)
    glVertex3f(1.0, -1.0, 1.0);  # Bottom Left Of The Quad (Right)
    glVertex3f(1.0, -1.0, -1.0);  # Bottom Right Of The Quad (Right)
    glEnd();  # Done Drawing The Quad


####################################################
# printf()
####################################################

def printf(format, *args):
    sys.stdout.write(format % args)


################################################################################
# start the app

print("Hit ESC key to quit.")
main()
